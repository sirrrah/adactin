using System;
using Adactin.Utilities;
using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Adactin.Application
{

    public class BookingPage
    {
        private readonly IWebDriver driver;
        private readonly ExtentTest test;

        public BookingPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public BookingPage(IWebDriver driver, ExtentTest test) : this(driver)
        {
            this.test = test;
        }

        /**
         * Fills in the booking information using the passed form values
         * @param firstName The user's first name
         * @param lastName The user's last name
         * @param address The user's residential address
         * @param cardNumber Current user's 16 Digit Credit Card Number
         * @param cardType Credit Card Type (i.e. AMEX, VISA, MAST, OTHR)
         * @param expMonth Credit Card Expiry Month
         * @param expYear Credit Card Expiry Year
         * @param cvv Credit Card CVV Number, maximum of four digits
         */
        public void BookAHotel(string firstName, string lastName, string address, string cardNumber,
                string cardType, string expMonth, string expYear, string cvv)
        {
            driver.FindElement(By.Id("first_name")).SendKeys(firstName);
            driver.FindElement(By.Id("last_name")).SendKeys(lastName);
            driver.FindElement(By.Id("address")).SendKeys(address);
            driver.FindElement(By.Id("cc_num")).SendKeys(cardNumber);
            new SelectElement(driver.FindElement(By.Id("cc_type"))).SelectByValue(cardType);
            new SelectElement(driver.FindElement(By.Id("cc_exp_month"))).SelectByValue(expMonth);
            new SelectElement(driver.FindElement(By.Id("cc_exp_year"))).SelectByValue(expYear);
            driver.FindElement(By.Id("cc_cvv")).SendKeys(cvv);

            Screenshot screenshot_bookHotel = ((ITakesScreenshot)driver).GetScreenshot();
            string path = Screenshots.GetPath() + @"\book-hotel-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            screenshot_bookHotel.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "hotel booking screenshot");
            test.Pass("Hotel booking successful");

            driver.FindElement(By.Id("book_now")).Click();
        }
    }
}