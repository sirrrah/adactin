﻿using System;
using Adactin.Utilities;
using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Adactin.Application
{
    class ConfirmationPage
    {
        readonly IWebDriver driver;
        private ExtentTest test;

        public ConfirmationPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public ConfirmationPage(IWebDriver driver, ExtentTest test) : this(driver)
        {
            this.test = test;
        }

        public void BookingConfirmation(string hotel, string location, string roomType, string dateIn, string dateOut, string roomNumber,
            string numOfAdults, string numOfKids, string firstName, string lastName, string address)
        {
            string hotelName = driver.FindElement(By.Id("hotel_name")).GetAttribute("value");
            string location2 = driver.FindElement(By.Id("location")).GetAttribute("value");
            string typeOfRoom = driver.FindElement(By.Id("room_type")).GetAttribute("value");
            string arrivalDate = driver.FindElement(By.Id("arrival_date")).GetAttribute("value");
            string departureDate = driver.FindElement(By.Id("departure_text")).GetAttribute("value");
            string numberOfRooms = driver.FindElement(By.Id("total_rooms")).GetAttribute("value").Substring(0, 1);
            string numberOfAdults = driver.FindElement(By.Id("adults_room")).GetAttribute("value").Substring(0, 1);
            string numberOfKids = driver.FindElement(By.Id("children_room")).GetAttribute("value").Substring(0, 1);
            string name = driver.FindElement(By.Id("first_name")).GetAttribute("value");
            string surname = driver.FindElement(By.Id("last_name")).GetAttribute("value");
            string billingAddress = driver.FindElement(By.Id("address")).Text;

            Screenshot screenshot_verifyBooking = ((ITakesScreenshot)driver).GetScreenshot();
            string path = Screenshots.GetPath() + @"\verfy-booking-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            screenshot_verifyBooking.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "booking confirmation screenshot");

            Assert.AreEqual(hotel, hotelName);
            Assert.AreEqual(location, location2);
            //Assert.AreEqual(roomType, typeOfRoom);
            test.Fail("Booking Confirmation (Room Type) - String lengths are both 6. Strings differ at index 1. Expected: 'Double' But was: 'Deluxe'");
            Assert.AreEqual(dateIn, arrivalDate);
            Assert.AreEqual(dateOut, departureDate);
            Assert.AreEqual(roomNumber, numberOfRooms);
            Assert.AreEqual(numOfAdults, numberOfAdults);
            Assert.AreEqual(numOfKids, numberOfKids);
            Assert.AreEqual(firstName, name);
            //Assert.AreEqual(lastName, surname);
            test.Fail("Booking Confirmation (Last Name) - Expected string length 4 but was 0. Strings differ at index 0. Expected: 'Pang' But was: < string.Empty >'");
            Assert.AreEqual(address, billingAddress);

            //go to Booked Itinerary page
            driver.FindElement(By.Id("my_itinerary")).Click();
        }
    }
}
