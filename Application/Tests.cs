﻿using System;
using Adactin.Data;
using Adactin.Utilities;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Adactin.Application
{
    //valid login/booking test with chrome
    [TestFixture]
    //[Parallelizable]
    class BookHotel
    {
        IWebDriver driver;
        readonly AventStack.ExtentReports.ExtentReports extent = new AventStack.ExtentReports.ExtentReports();

        [SetUp]
        public void SetUp()
        {
            driver = DriverFactory.Set("chrome");
            //driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }

        [Test]
        public void TestCase_01()
        {
            ExcelData.PopulateInCollection(@"A:\Work\Visual Studio\Adactin\Data\test_data.xlsx");

            string url = "http://adactinhotelapp.com/index.php";
            string username = ExcelData.ReadData(1, "username");
            string password = ExcelData.ReadData(1, "password");

            string hotel = ExcelData.ReadData(1, "Hotel");
            string location = ExcelData.ReadData(1, "Location");
            string roomType = ExcelData.ReadData(1, "Room Type");
            string roomNumber = ExcelData.ReadData(1, "Room Number");
            string dateIn = ExcelData.ReadData(1, "Date In");
            string dateOut = ExcelData.ReadData(1, "Date Out");
            string numOfAdults = ExcelData.ReadData(1, "No. of Adults");
            string numOfKids = ExcelData.ReadData(1, "No. of Kids");
            string firstName = ExcelData.ReadData(1, "First Name");
            string lastName = ExcelData.ReadData(1, "Last Name");
            string address = ExcelData.ReadData(1, "Address");
            string cardNumber = ExcelData.ReadData(1, "Card Number");
            string cardType = ExcelData.ReadData(1, "Card Type");
            string expMonth = ExcelData.ReadData(1, "Exp Month");
            string expYear = ExcelData.ReadData(1, "Exp Year");
            string cvv = ExcelData.ReadData(1, "CVV");

            //Extent Report declaration
            ExtentHtmlReporter reporter = new ExtentHtmlReporter(@"A:\Work\Visual Studio\Adactin\Reporting\test1\TC01_Extent_Report.html");
            extent.AttachReporter(reporter);
            ExtentTest test = extent.CreateTest("AddactIn Hotel Test Case - 01", "Test Using Chrome Browser and Valid Login Credentials");

            //Visit Website
            test.Log(Status.Info, "Execute test case 01");
            PageNavigation pageNav = new PageNavigation(driver, test);
            pageNav.GoTo(url);
            test.Pass("open URL (" + url + ")");

            //login information
            LoginPage loginPage = new LoginPage(driver);
            loginPage.SetUsername(username);
            loginPage.SetPassword(password);
            string path = Screenshots.GetPath() + @"\login-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            Screenshot screenshot_login = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot_login.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "login screenshot");
            test.Pass("username and password correct");
            loginPage.ClickSubmit();

            //confirm login
            Assert.IsTrue(driver.FindElement(By.Id("username_show")).GetAttribute("value").Contains("Hello"));

            //search for hotels
            SearchPage searchPage = new SearchPage(driver, test);
            searchPage.FullHotelSearch(location, hotel, roomType, roomNumber, dateIn, dateOut, numOfAdults, numOfKids);

            //select a hotel
            SelectPage selectPage = new SelectPage(driver, test);
            selectPage.SelectHotel(0);//select first row of the searched results

            BookingPage bookingPage = new BookingPage(driver, test);
            //passed parameters: firstName, lastName, address, cardNumber, cardType, expMonth, expYear, cvv
            bookingPage.BookAHotel(firstName, lastName, address, cardNumber, cardType, expMonth, expYear, cvv);
            
            ConfirmationPage confirmationPage = new ConfirmationPage(driver, test);
            //passed parameters: hotel, location, roomType, dateIn, dateOut, roomNumber, numOfAdults, numOfKids, firstName, lastName, address
            confirmationPage.BookingConfirmation(hotel, location, roomType, dateIn, dateOut, roomNumber, numOfAdults, numOfKids, firstName, lastName, address);

            //logout
            pageNav.Logout();
        }

        [TearDown]
        public void TearDown()
        {
            extent.Flush();
            driver.Quit();
        }
    }

    //invalid login test with firefox 
    [TestFixture]
    //[Parallelizable]
    class BookHotelWithFirefox
    {
        IWebDriver driver;
        readonly AventStack.ExtentReports.ExtentReports extent = new AventStack.ExtentReports.ExtentReports();

        [SetUp]
        public void SetUp()
        {
            driver = DriverFactory.Set("firefox");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }

        [Test]
        public void TestCase_02()
        {
            ExcelData.PopulateInCollection(@"A:\Work\Visual Studio\Adactin\Data\test_data.xlsx");

            string username = ExcelData.ReadData(2, "username");
            string password = ExcelData.ReadData(2, "password");

            //Extent Report declaration
            ExtentHtmlReporter reporter = new ExtentHtmlReporter(@"A:\Work\Visual Studio\Adactin\Reporting\test2\TC02_Extent_Report.html");
            extent.AttachReporter(reporter);
            ExtentTest test = extent.CreateTest("AddactIn Hotel Test Case - 02", "Test Using Firefox Browser and Invalid Login Credentials");

            //Visit Website
            PageNavigation pageNav = new PageNavigation(driver);
            pageNav.GoTo("http://adactinhotelapp.com/index.php");

            //login information and test for invalid login
            LoginPage loginPage = new LoginPage(driver);
            loginPage.SetUsername(username);
            loginPage.SetPassword(password);
            loginPage.ClickSubmit();
            
            string path = Screenshots.GetPath() + @"\login-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            Screenshot screenshot_login = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot_login.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "login screenshot");
            test.Fail("username or password incorrect");

            //confirm login
            Assert.IsTrue(driver.FindElement(By.ClassName("auth_error")).Displayed);

        }

        [TearDown]
        public void TearDown()
        {
            extent.Flush();
            driver.Quit();
        }
    }
}
