﻿using System.IO;

namespace Adactin.Utilities
{
    public class Screenshots
    {
        public static string GetPath()
        {
            //screenshots directory
            string screenshots = Directory.GetCurrentDirectory() + @"\screenshots";
            if (!Directory.Exists(screenshots))
            {
                Directory.CreateDirectory(screenshots);
            }

            return screenshots;
        }
    }
}
