﻿using OpenQA.Selenium;

namespace Adactin.Application
{
    public class LoginPage
    {
        readonly IWebDriver driver;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void SetUsername(string username)
        {
            driver.FindElement(By.Id("username")).SendKeys(username);
        }

        public void SetPassword(string password)
        {
            driver.FindElement(By.Id("password")).SendKeys(password);
        }

        public void ClickSubmit()
        {
            driver.FindElement(By.Id("login")).Click();
        }
    }
}
