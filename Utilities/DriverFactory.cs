﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;

namespace Adactin.Utilities
{
    public class DriverFactory
    {
        public static IWebDriver Set(string browser)
        {
            switch (browser.ToLower())
            {
                case "chrome"://Google Chrome
                    return new ChromeDriver();
                case "firefox"://Mozilla Firefox
                    return new FirefoxDriver();
                case "edge"://Microsoft Edge Legacy
                    return new EdgeDriver();
                default:
                    return Set("chrome");
            }
        }
    }
}
