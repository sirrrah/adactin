﻿using System;
using Adactin.Utilities;
using AventStack.ExtentReports;
using OpenQA.Selenium;

namespace Adactin.Application
{
    public class SelectPage
    {
        private readonly IWebDriver driver;
        private readonly ExtentTest test;

        public SelectPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public SelectPage(IWebDriver driver, ExtentTest test) : this(driver)
        {
            this.test = test;
        }

        /**
         * Selects a hotel using the row index from the hotel search results
         * @param row The index of the row to select
         */
        public void SelectHotel(int row)
        {
            int rows = driver.FindElements(By.CssSelector("html > body > table.content > tbody > tr > td > form#select_form > table.login > tbody > tr > td > table > tbody > tr")).Count;

            if (rows == 0)
            {
                Console.WriteLine("Nothing to select!");
            }
            else
            {
                if (row >= 0 && row <= rows)
                {
                    driver.FindElement(By.Id("radiobutton_" + row)).Click();
                    Screenshot screenshot_selectHotel = ((ITakesScreenshot)driver).GetScreenshot();
                    string path = Screenshots.GetPath() + @"\select-hotel-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
                    screenshot_selectHotel.SaveAsFile(path, ScreenshotImageFormat.Png);
                    test.AddScreenCaptureFromPath(path, "selection screenshot");
                    test.Pass("Hotel selection successful");
                    driver.FindElement(By.Id("continue")).Click();
                }
                else
                {
                    Console.WriteLine("Row not found!");
                }
            }
        }
    }

}
