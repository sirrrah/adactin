﻿using System;
using Adactin.Utilities;
using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Adactin.Application
{
    public class SearchPage
    {
        private readonly IWebDriver driver;
        private readonly ExtentTest test;

        public SearchPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public SearchPage(IWebDriver driver, ExtentTest test) : this(driver)
        {
            this.test = test;
        }

        /**
        *Search hotels using only the mandatory 
        *fields marked with a red asterisk. Only the 
        *location field needs to be given a value, 
        *others are set by default.
        */
        public void SimpleHotelSearch()
        {
            SelectElement dropdown = new SelectElement(driver.FindElement(By.Id("location")));
            dropdown.SelectByValue("Sydney");

            string path = Screenshots.GetPath() + @"\search-hotel-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            Screenshot screenshot_searchHotel = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot_searchHotel.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "search screenshot");
            test.Pass("Hotel search successful");

            driver.FindElement(By.Id("Submit")).Click();
        }

        /**
         * Search hotels using all the form fields
         * @param location The city of the hotel being searched (i.e. Sydney, Melbourne, Brisbane, Adelaide, London, New York, Los Angeles, or Paris)
         * @param hotel The name of the hotel (i.e. Hotel Creek, Hotel Sunshine, Hotel Hervey, or Hotel Cornice)
         * @param roomType The type of the room (i.e. Standard, Double, Deluxe, or Super Deluxe) 
         * @param numberOfRooms The number of rooms (from 1 to 10)
         * @param dateIn The check in date in the format (dd/mm/yyyy)
         * @param dateOut The check in date out the format (dd/mm/yyyy)
         * @param numberOfAdults The number of adults (from 1 to 4)
         * @param numberOfKids The number of children (from 1 to 4)
         */
        public void FullHotelSearch(string location, string hotel, string roomType,
                string numberOfRooms, string dateIn, string dateOut, string numberOfAdults, string numberOfKids)
        {

            new SelectElement(driver.FindElement(By.Id("location"))).SelectByValue(location);
            new SelectElement(driver.FindElement(By.Id("hotels"))).SelectByValue(hotel);
            new SelectElement(driver.FindElement(By.Id("room_type"))).SelectByValue(roomType);
            new SelectElement(driver.FindElement(By.Id("room_nos"))).SelectByValue(numberOfRooms);
            driver.FindElement(By.Id("datepick_in")).Clear();
            driver.FindElement(By.Id("datepick_in")).SendKeys(dateIn);
            driver.FindElement(By.Id("datepick_out")).Clear();
            driver.FindElement(By.Id("datepick_out")).SendKeys(dateOut);
            new SelectElement(driver.FindElement(By.Id("adult_room"))).SelectByValue(numberOfAdults);
            new SelectElement(driver.FindElement(By.Id("child_room"))).SelectByValue(numberOfKids);

            string path = Screenshots.GetPath() + @"\search-hotel-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            Screenshot screenshot_searchHotel = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot_searchHotel.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "search screenshot");
            test.Pass("Hotel search successful");

            driver.FindElement(By.Id("Submit")).Click();
        }
    }
}
