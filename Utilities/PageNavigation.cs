﻿using System;
using AventStack.ExtentReports;
using OpenQA.Selenium;

namespace Adactin.Utilities
{
    public class PageNavigation
    {
        private readonly IWebDriver driver;
        private ExtentTest test;

        public PageNavigation(IWebDriver driver)
        {
            this.driver = driver;
        }

        public PageNavigation(IWebDriver driver, ExtentTest test) : this(driver)
        {
            this.test = test;
        }

        public void GoTo(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public void Logout()
        {
            driver.FindElement(By.LinkText("Logout")).Click();
            Screenshot screenshot_logout = ((ITakesScreenshot)driver).GetScreenshot();
            string path = Screenshots.GetPath() + @"\logout-screenshot" + DateTime.Now.ToString("MM-dd-yy_Hmm") + ".png";
            screenshot_logout.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.AddScreenCaptureFromPath(path, "logout screenshot");
            test.Pass("logout successful");
        }
    }
}

